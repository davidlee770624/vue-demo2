# test

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### 建立VUE+WebPack專案
https://medium.com/@des75421/vue-%E7%94%A8-vue-cli-%E5%BB%BA%E7%AB%8B%E6%96%B0%E5%B0%88%E6%A1%88-2d75e8e4e43d
[待電腦重啟後再執行]
#npm install --global --production windows-build-tools這條命令

[安裝webpack]
#npm i -g webpack@latest webpack-cli@latest

[安裝@vue-cli 3]
#npm uninstall vue-cli -g
#npm install -g @vue/cli@latest

[使用vue-cli 3建立vue專案]
#vue create <專案名稱> 
#自選選項 router vuex , 取消linter
#cd 專案資料夾
#npm install
#npm run dev

[使用vue-cli 2建立vue專案]
$ vue init <樣板名稱> <專案名稱>  
#vue init webpack 專案資料夾
#cd 專案資料夾
#npm install
#npm run dev

[python錯誤]
管理員執行 #npm install --global --production windows-build-tools

[更新module]
#npm update

### AXIOS套件,取得資料用
#npm install axios vue-axios -D
