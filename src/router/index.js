import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue';
import About from '../views/About.vue';
import AboutVuex from '../views/AboutVuex.vue';
import Header from '../components/Header.vue';

// 頁面路由器

Vue.use(VueRouter)
const routes = [
  {
    path: '/',
    name: 'Home',
    components: {
      default: Home,
      nav: Header // 有需要header的頁面
    }
  },
  {
    path: '/about',
    name: 'About',
    components: {
      default: About, 
      nav: Header // 有需要header的頁面
    }
  },
  {
    path: '/AboutVuex',
    name: 'AboutVuex',
    components: {
      default: AboutVuex,
      nav: Header  // 有需要header的頁面
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '*',
    redirect: '/',
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

// 驗證登入token
router.beforeEach((to, from, next)=>{
  const isLogin = localStorage.getItem('token') == 'Y' ;
  if( isLogin ){
    next();
  } else {
    if( to.path !== '/login')
      next('/login');
    else
      next();
  }
});

export default router
