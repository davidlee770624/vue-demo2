// build專案的起始點
// 在此引用共用JS套件, 例如axios
import Vue from 'vue'
import axios from 'axios' // 從node_module取得
import VueAxios from 'vue-axios'
import App from './App.vue' // 從本地取得
import router from './router'
import store from './store'

// 在此引用共用SCSS套件 , 例如bootstrap
import './assets/scss/main.scss'
// 直接從模組
// import bootstrap from  "bootstrap/scss/bootstrap.scss";

Vue.config.productionTip = false
Vue.use(VueAxios,axios) // 使用它,vue才能直接使用

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
