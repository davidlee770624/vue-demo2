import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
// https://github.com/Wcc723/170622_vuex_demo.git
export default new Vuex.Store({
  state: {
    vuexStatus: '線上!',
    vuexUsername: 'David!'
  },

  // 修改state
  mutations: {
  },

  // 取得資料 , commit傳送給mutations
  actions: {
  },
  modules: {
  }
})
